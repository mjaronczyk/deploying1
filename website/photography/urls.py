from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('services', views.services, name='services'),
    path('gallery', views.gallery, name='gallery'),
    path('contacts', views.contacts, name='contacts'),
    path('gallery1', views.gallery_ciaze, name='gallery1'),
    path('gallery2', views.gallery_noworodki, name='gallery2'),
    path('gallery3', views.gallery_noworodki, name='gallery3'),
    path('gallery4', views.gallery_noworodki, name='gallery4'),

]
