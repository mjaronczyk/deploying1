from django.shortcuts import render, redirect
from django.core.mail import send_mail
from .shared import Operation
from .models import Counter, Details, Message




def index(request):
    return render(request, 'index.html')


def about(request):
    counters = Counter.objects.all()
    return render(request, 'about-me.html', {'counters': counters})


def services(request):
    return render(request, 'services.html')


def gallery(request):
    return render(request, 'gallery.html')


def contacts(request):
    name = False
    error = False
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        text = request.POST['textarea']
        message = f'Imię Użytkownika:\n{name}\n\n {text} \n\n Email Adres Użytkownika: {email}'
        if name and email and text:
            message_db = Message(email=email, comment=text, name=name, is_read=0)
            message_db.save()
            # send_mail(
            #     "Wiadomość Od Użytkownika",
            #         message,
            #     'm.jaronczyk@interia.pl',
            #     ['m.jaronczyk@interia.pl'],
            #     fail_silently=True,)
            return render(request, 'contacts.html', {'name': name, 'error': error})
        else:
            error = True
            return render(request, 'contacts.html', {'error': error})
    return render(request, 'contacts.html')



def gallery_ciaze(request):
    images, path = Operation().get_images_from_path('/static/images/ciaze_img/')
    return render(request, 'blog-gallery-album-ciaze.html', {'images': images, 'path': path})


def gallery_noworodki(request):
    images, path = Operation().get_images_from_path('/static/images/noworodki_img/')
    return render(request, 'blog-gallery-album-ciaze.html', {'images': images, 'path': path})

def gallery3(request):
    images, path = Operation().get_images_from_path('/static/images/ciaze_img/')
    return render(request, 'blog-gallery-album-ciaze.html', {'images': images, 'path': path})


def gallery4(request):
    images, path = Operation().get_images_from_path('/static/images/ciaze_img/')
    return render(request, 'blog-gallery-album-ciaze.html', {'images': images, 'path': path})
