from django.db import models


class Counter(models.Model):
    section = models.CharField(max_length=80, unique=True, editable=False)
    value = models.IntegerField()

    def __str__(self):
        return self.section



class Details(models.Model):
    email = models.EmailField(null=True, blank=True)
    telefon = models.CharField(max_length=30, null=True, blank=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    

class Message(models.Model):
    date = models.DateTimeField(auto_now=True)
    email = models.EmailField()
    comment = models.TextField(max_length=10)
    name = models.CharField(max_length=20)
    is_read = models.IntegerField()

