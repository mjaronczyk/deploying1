from django import template
from datetime import datetime


register = template.Library()

@register.filter
def modulo(num, val):
    return num % val




@register.filter
def index_value(list, index):
    return list[index].value
