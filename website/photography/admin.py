
from django.contrib import admin
from django.contrib.auth.models import User, Group
from .models import *

admin.site.unregister(User)
admin.site.unregister(Group)



@admin.register(Counter)
class CounterAdmin(admin.ModelAdmin):
    actions = None
    list_display = ('section', 'value')

    def has_add_permission(self, request, obj=None):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False



@admin.register(Details)
class DetailsAdmin(admin.ModelAdmin):
    actions = None
    list_display = ('email', 'telefon', 'address',)

    def has_add_permission(self, request, obj=None):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False



@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('date', 'name', 'email', 'comment',)
    

    def has_add_permission(self, request, obj=None):
        return False




