import os
import pathlib

class Operation:

    def get_images_from_path(self, rest_path):
        absolute_path = os.path.dirname(__file__)
        absolute_path = str(pathlib.Path(absolute_path).parent)
        path = absolute_path + rest_path
        path = path.replace("\\","/")
        print(path)
        return os.listdir(path), rest_path
        